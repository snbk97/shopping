<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="stylesheet" type="text/css" href="css/materialize.min.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>Greater Noida Bazaar</title>
</head>
<body>
    <!-- Nav starts here-->
    <ul id="dropdown1" class="dropdown-content" style="margin-top: 4.2em;">
      <li><a href="">Service one</a></li>
      <li><a href="#!">Service two</a></li>
      <li><a href="#!">Service three</a></li>
    </ul>
    <ul id="dropdown2" class="dropdown-content" style="margin-top: 3em;">
      <li><a href="#!">Service one</a></li>
      <li><a href="#!">Service two</a></li>
      <li><a href="#!">Service three</a></li>
    </ul>

        <div class="navbar-fixed ">
        <nav class="brown darken-3">
            <div class="container">
                <div class="nav-wrapper">
                <a href="#!" class="brand-logo">Logo</a>
                <a href="#" data-activates="mobile-menu" class="button-collapse"><i class="material-icons">menu</i></a>
                <ul class="right hide-on-med-and-down">
                    <li><a href="index.html">Home</a></li>
                    <li><a href="products.php">Products</a></li>
                    <li><a class="dropdown-button" href="#!" data-activates="dropdown1">Services<i class="material-icons right">arrow_drop_down</i></a></li>
                    <li><a href="collapsible.html">How it works</a></li>
                    <li><a href="collapsible.html">Rate Us</a></li>
                    <li><a href="contact.php">Contact us</a></li>
                    <li><a href="mobile.html">Mobile</a></li>
                </ul>
                </div>

                <div class="nav-content">
                      <a class="btn-floating btn-large halfway-fab waves-effect waves-light orange darken-1">
                        <i class="material-icons tooltipped" data-tooltip="Go to basket">shopping_cart</i>
                      </a>
                </div>

            </div>
        </nav>
        </div>
        <ul class="side-nav" id="mobile-menu">
            <li><a href="index.html">Home</a></li>
                    <li><a href="products.php">Products</a></li>
                    <li><a class="dropdown-button" href="#!" data-activates="dropdown2">Services<i class="material-icons right">arrow_drop_down</i></a></li>
                    <li><a href="collapsible.html">How it works</a></li>
                    <li><a href="collapsible.html">Rate Us</a></li>
                    <li><a href="contact.php">Contact us</a></li>
                    <li><a href="mobile.html">Mobile</a></li>
        </ul>

    <!-- Nav ends here-->
    <div class="spacer"></div>
   

    <div class="row">
      <form class="col s12">
        <div class="row">
          <div class="input-field col s6">
            <input placeholder="Placeholder" id="first_name" type="text" class="validate">
            <label for="first_name">First Name</label>
          </div>
          <div class="input-field col s6">
            <input placeholder="Last name"id="last_name" type="text" class="validate">
            <!-- <label for="last_name">Last Name</label> -->
          </div>
        </div>
        <div class="row">
          <div class="input-field col s12">
            <input disabled value="I am not editable" id="disabled" type="text" class="validate">
            <label for="disabled">Disabled</label>
          </div>
        </div>
        <div class="row">
          <div class="input-field col s12">
            <input id="password" type="password" class="validate">
            <label for="password">Password</label>
          </div>
        </div>
        <div class="row">
          <div class="input-field col s12">
            <input id="email" type="email" class="validate">
            <label for="email">Email</label>
          </div>
        </div>
        <div class="row">
          <div class="col s12">
            This is an inline input field:
            <div class="input-field inline">
              <input id="email" type="email" class="validate">
              <label for="email" data-error="wrong" data-success="right">Email</label>
            </div>
          </div>
        </div>
      </form>
    </div>
          


</body>
<script type="text/javascript">
      $(document).ready(function() {
    Materialize.updateTextFields();
  });
</script>
</html>