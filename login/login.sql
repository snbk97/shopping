-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 09, 2017 at 03:09 AM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `login`
--

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `p_id` varchar(5) NOT NULL,
  `p_name` varchar(100) NOT NULL,
  `p_desc` varchar(300) NOT NULL,
  `p_cat` varchar(50) NOT NULL,
  `p_rate` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`p_id`, `p_name`, `p_desc`, `p_cat`, `p_rate`) VALUES
('P0001', 'Bru', '200gm', 'Tea & Cofee', 80),
('P0002', 'Maggie Noodles', '4 pack', 'Ready to cook', 40),
('P0003', 'Top Ramen', '2 pack', 'Snacks', 70),
('P0004', 'Maggie Noodles', '2 pack', 'Snacks', 50),
('P0005', 'Britannia Muffin', '1 pack', 'Snacks', 15),
('P0006', 'Knorr Soupy Noodes', '1 pack', 'Snacks', 30),
('P0007', 'Monginis Muffins', '4 pack', 'Snacks', 50),
('P0008', 'Mountain Dew', '600ml', 'Water & Beverages', 40),
('P0009', 'Pepsi', '600ml', 'Water & Beverages', 42),
('P0010', 'Litchi Paper Boat', '250ml', 'Water & Beverages', 20);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `uname` varchar(10) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `pass_hash` longtext NOT NULL,
  `phone` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`uname`, `name`, `email`, `pass_hash`, `phone`) VALUES
('snbk97', 'Sayan Bhowmik', 'snbk97@gmail.com', 'F30AA7A662C728B7407C54AE6BFD27D1', '8285188034'),
('v0idmain', 'Aakash Suresh', 'aakash.ps99@gmail.com', 'F30AA7A662C728B7407C54AE6BFD27D1', '9182723842'),
('synyster20', 'Sahil Tiwari', 'sahilsuxx@gmail.com', 'F30AA7A662C728B7407C54AE6BFD27D1', '8912348237');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
