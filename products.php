<?php 
	$db = new mysqli("localhost", "root", "", "login");
	if ($db->connect_error) {
	    die('Connect Error: ' . $mysqli->connect_error);
	}

 ?>

<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="stylesheet" type="text/css" href="css/materialize.min.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>Document</title>
</head>
<body>
	<!-- Nav starts here-->
	<ul id="dropdown1" class="dropdown-content" style="margin-top: 4.2em;">
	  <li><a href="">Service one</a></li>
	  <li><a href="#!">Service two</a></li>
	  <li><a href="#!">Service three</a></li>
	</ul>
	<ul id="dropdown2" class="dropdown-content" style="margin-top: 3em;">
	  <li><a href="#!">Service one</a></li>
	  <li><a href="#!">Service two</a></li>
	  <li><a href="#!">Service three</a></li>
	</ul>

	    <div class="navbar-fixed ">
	    <nav class="brown darken-3">
	        <div class="container">
	            <div class="nav-wrapper">
	            <a href="#!" class="brand-logo">Logo</a>
	            <a href="#" data-activates="mobile-menu" class="button-collapse"><i class="material-icons">menu</i></a>
	            <ul class="right hide-on-med-and-down">
	                <li><a href="index.html">Home</a></li>
	                <li><a href="products.php">Products</a></li>
	                <li><a class="dropdown-button" href="#!" data-activates="dropdown1">Services<i class="material-icons right">arrow_drop_down</i></a></li>
	                <li><a href="collapsible.html">How it works</a></li>
	                <li><a href="collapsible.html">Rate Us</a></li>
	                <li><a href="collapsible.html">Contact us</a></li>
	                <li><a href="mobile.html">Mobile</a></li>
	            </ul>
	            </div>

	            <div class="nav-content">
	                  <a class="btn-floating btn-large halfway-fab waves-effect waves-light orange darken-1">
	                    <i class="material-icons tooltipped" data-tooltip="Go to basket">shopping_cart</i>
	                  </a>
	            </div>

	        </div>
	    </nav>
	    </div>
	    <ul class="side-nav" id="mobile-menu">
	        <li><a href="sass.html">Home</a></li>
	                <li><a href="badges.html">Products</a></li>
	                <li><a class="dropdown-button" href="#!" data-activates="dropdown2">Services<i class="material-icons right">arrow_drop_down</i></a></li>
	                <li><a href="collapsible.html">How it works</a></li>
	                <li><a href="collapsible.html">Rate Us</a></li>
	                <li><a href="collapsible.html">Contact us</a></li>
	                <li><a href="mobile.html">Mobile</a></li>
	    </ul>
	<!-- Nav ends here-->

	<div class="spacer"></div>
    <button id="btn">Clickme</button>
    <script type="text/javascript">
        btn = document.getElementById('btn')
        btn.onclick = ()=>{
            document.getElementsByClass
        }
    </script>

	<div class="container">
		
            <?php
                getProductByCategory($db,'Snacks');
            ?>
		

       
            <?php
                getProductByCategory($db,'Water & Beverages');
            ?>

	</div>

</body>
</html>
<?php 
    function getProductByCategory($database, $category){
        $q = "SELECT * from `products` WHERE p_cat='$category'";
        $result = $database->query($q);
        // print_r($result);
        echo '<div class="spacer"></div>
                <div class="row">
                <h5>'.$category.'</h5>
                <div class="spacer"></div>';


        if($result->num_rows > 0){
            while($row = $result->fetch_assoc()){
                // print_r($row);
                echo '
                    <div class="col l3 m6 s6">
                        <div class="card-panel hoverable">
                            <span class="card-title">' .
                            $row['p_id'] . '&nbsp;&nbsp;' . $row['p_name'] . '&nbsp;&nbsp;' .$row['p_desc'] . 
                           '</span>
                        </div>
                    </div>
                    ';
            }
        } else {
            echo "None found";
        }

        echo '</div>';
    }
 ?>